using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRollTrigger : MonoBehaviour
{
    [SerializeField] float angleMin = 10;
    [SerializeField] float angleMax = 45;
    private bool isTriggered;


    // note: can use Action<bool> for 1 event
    public event Action OnRollRight;
    public event Action OnRollLeft;



    void Update()
    {
        float angle = this.transform.eulerAngles.z;
        angle = WrapAngle(angle);

        if (!isTriggered && Mathf.Abs(angle) > angleMax)
        {
            isTriggered = true;
            if (angle > 0) OnRollLeft ?.Invoke();
            if (angle < 0) OnRollRight?.Invoke();
        }
        else if (isTriggered && Mathf.Abs(angle) < angleMin)
        {
            isTriggered = false;
        }
    }


    static private float WrapAngle(float a)
    {
        // convert any angle to range [-180, +180]
        a %= 360f;
        return a < 180f ? a : a - 360f;
    }
}
