using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Animation))]
public class PlanetSwitcher : MonoBehaviour
{
    [SerializeField] CameraRollTrigger rollTrigger;
    [SerializeField] Transform planetsParent;
    [SerializeField] int startPlanetIdx;
    [SerializeField] AnimationClip nextClip;
    [SerializeField] AnimationClip prevClip;

    private Animation anim;
    private int currPlanetIdx;
    private int planetsCount;
    private bool switchToNextPlanet;


    void Awake()
    {
        rollTrigger.OnRollRight += PlayPrevAnimation;
        rollTrigger.OnRollLeft  += PlayNextAnimation;
        planetsCount = planetsParent.childCount;
        anim = this.GetComponent<Animation>();
    }


    void Start()
    {
        currPlanetIdx = startPlanetIdx;
        UpdateActivePlanet();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
            PlayNextAnimation();
        if (Input.GetKeyDown(KeyCode.P))
            PlayPrevAnimation();
    }


    private void PlayNextAnimation()
    {
        switchToNextPlanet = true;
        anim.Play(nextClip.name);
    }


    private void PlayPrevAnimation()
    {
        switchToNextPlanet = false;
        anim.Play(prevClip.name);
    }


    // NOTE: this func is called as Unity Animation Event
    // do not refactor it's name, overwise take care to re-select it from animation clips
    private void SwitchPlanet()
    {
        if (switchToNextPlanet) currPlanetIdx++;
        else                    currPlanetIdx--;
        // wrap index so it will not be out of bounds [0, planetCount)
        currPlanetIdx = (currPlanetIdx + planetsCount) % planetsCount;
        UpdateActivePlanet();
    }


    private void UpdateActivePlanet()
    {
        // deactivate all planets
        for (int i = 0; i < planetsCount; ++i)
            planetsParent.GetChild(i).gameObject.SetActive(false);
        // activate current
        planetsParent.GetChild(currPlanetIdx).gameObject.SetActive(true);
    }


}
