using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotator : MonoBehaviour
{

    [SerializeField] float speedDegrees = 10;
    [SerializeField] Vector3 localAxis = Vector3.forward;


    void Update()
    {
        float degrees = speedDegrees * Time.deltaTime;
        transform.Rotate(localAxis, degrees, Space.Self);
    }
}
