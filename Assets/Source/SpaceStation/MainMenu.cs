using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class MainMenu : MonoBehaviour
{
    // NOTE: this is called from Button's 'OnClick' event
    public void OnSomeButtonClick(string sceneName)
    {
        Debug.Assert(string.IsNullOrEmpty(sceneName) == false);
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
}
