using System.Collections;
using System.Collections.Generic;
using UnityEngine;



// NOTE: this script enable/disable is handled from Vuforia's OnTargetFound/OnTargetLost UnityEvents

public class SignPost : MonoBehaviour
{
    [SerializeField] TextMesh captionText;
    [SerializeField] TextMesh distanceText;
    [SerializeField] Transform xfSign;

    private Destination destination;


    //............................................................UNITY

    void OnValidate()
    {
        Debug.Assert(captionText  != null, this);
        Debug.Assert(distanceText != null, this);
        Debug.Assert(xfSign       != null, this);
    }


    void Update()
    {
        bool hasDestination = destination != null;
        SetTextVisible(hasDestination);
        if (hasDestination)
        {
            captionText.text = destination.caption;
            distanceText.text = GetDestinationDistanceText();
            PointSignToDestination();
        }
    }

    //............................................................PRIVATE

    private string GetDestinationDistanceText()
    {
        Debug.Assert(destination != null);
        Vector3 p1 = this.transform.position;
        Vector3 p2 = destination.transform.position;
        float distance = (p1 - p2).magnitude;
        distance *= 100f; // convert meters to cm
        return string.Format("{0:#.##} cm", distance);
    }


    private void SetTextVisible(bool visible)
    {
        captionText.gameObject.SetActive(visible);
        distanceText.gameObject.SetActive(visible);
    }


    private void PointSignToDestination()
    {
        xfSign.LookAt(destination.transform, Vector3.up);
    }


    //..............................................called as UnityEvent

    public void OnDestinationFound(Destination dest)
    {
        destination = dest;
    }


    public void OnDestinationLost()
    {
        destination = null;
    }
}
