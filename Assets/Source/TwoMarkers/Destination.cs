using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destination : MonoBehaviour
{
    public string caption;


    void OnValidate()
    {
        Debug.Assert(string.IsNullOrEmpty(caption) == false, this);
    }
}
