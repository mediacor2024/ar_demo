using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VuforiaStartup : MonoBehaviour
{
    [SerializeField] int maxTrackedTargets = 1;

    void Start()
    {
        VuforiaApplication.Instance.OnVuforiaInitialized += VuforiaInitialized;
        VuforiaConfiguration.Instance.Vuforia.MaxSimultaneousImageTargets = maxTrackedTargets;
    }

    void VuforiaInitialized(VuforiaInitError err)
    {
        Debug.Log("Vuforia inited with error code: " + err);
        VuforiaBehaviour.Instance.CameraDevice.SetFocusMode(FocusMode.FOCUS_MODE_MACRO);
    }
}
